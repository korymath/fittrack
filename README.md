FitTrack
================================

FitTrack is a WordPress plugin thats adds fitness tracking capabilities to your WordPress website. You can get detailed reports of your workouts and chart your progress.

Demo site: http://korymathewson.com/fittrack


