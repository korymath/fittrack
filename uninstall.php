<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   FitTrack
 * @author    Kory Mathewson <kory@madebyflagship.com>
 * @license   GPL-2.0+
 * @link      http://korymathewson.com/fittrack
 * @copyright 2013 Made by Flagship
 */

// If uninstall not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// @TODO: Define uninstall functionality here