=== FitTrack ===

Contributors: korymath
Donate link: http://madebyflagship.com/
Tags: fitness, workout, chart
Requires at least: 3.7.1
Tested up to: 3.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin adds fitness tracking capabilities to your WordPress website. You can get detailed reports of your workouts and chart your progress.

== Description ==

This plugin adds fitness tracking capabilities to your WordPress website. You can get detailed reports of your workouts and chart your progress.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `fittrack` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

= What is this plugin all about? =

This plugin adds fitness tracking capabilities to your WordPress website.

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets 
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png` 
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* We are just getting started.

Here's a link to [WordPress](http://wordpress.org/ "Your favorite software") and one to [Markdown's Syntax Documentation][markdown syntax].
