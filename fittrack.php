<?php
/**
 * The WordPress Plugin Boilerplate.
 *
 * A foundation off of which to build well-documented WordPress plugins that
 * also follow WordPress Coding Standards and PHP best practices.
 *
 * @package   FitTrack
 * @author    Kory Mathewson <kory@madebyflagship.com>
 * @license   GPL-2.0+
 * @link      http://korymathewson.com/fittrack
 * @copyright 2013 Made by Flagship
 *
 * @wordpress-plugin
 * Plugin Name:       FitTrack
 * Plugin URI:        http://korymathewson.com/fittrack
 * Description:       This plugin adds fitness tracking capabilities to your WordPress website. You can get detailed reports of your workouts and chart your progress.
 * Version:           1.0.0
 * Author:            Kory Mathewson
 * Author URI:        http://madebyflagship.com
 * Text Domain:       fittrack
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

function fittrack_page_content_shortcode() {
	
	ob_start();
 
	echo '<div>';

	echo '<h1>Tracked workouts:</h1>';
	// List the tracked workouts
	
	echo '<h1>Track a new workout:</h1>';
	// Add a form for workout tracking.

	echo '</div>';
	
	return ob_get_clean();

}
add_shortcode('fittrack_main', 'fittrack_page_content_shortcode');
